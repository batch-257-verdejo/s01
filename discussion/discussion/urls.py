from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    # By using 'include' we are attaching ALL the urls from a specific package to the specific path that is declared in this file
    # Example: in order to see the index page of todolist package we have to visit this url: localhost:8000/todolist/
    path('todolist/', include('todolist.urls')),
    path('admin/', admin.site.urls),
]
